Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Forge
Upstream-Contact: https://github.com/digitalbazaar/forge/issues
 <support@digitalbazaar.com>
Source: https://github.com/digitalbazaar/forge/releases
 https://github.com/digitalbazaar/forge.git
 git://github.com/digitalbazaar/forge.git
 Repackaged, excluding potentially DFSG-nonfree binary code.
Files-Excluded:
 flash/swf/SocketPool.swf

Files: *
Copyright:
  2008-2019  Digital Bazaar, Inc. <support@digitalbazaar.com>
  2014       Lautaro Cozzani <lautaro.cozzani@scytl.com>
  2001       Paul Tero <http://www.tero.co.uk/des/>
  2012       Stefan Siegl <stesie@brokenpipe.de>
License-Grant:
 You may use the Forge project
 under the terms of either the BSD License
 or the GNU General Public License (GPL) Version 2.
License: BSD-3-Clause or GPL-2
Reference:
 LICENSE
 package.json
Comment:
 Licensing granted in files <LICENSE>
 and referenced in <package.json>
 is assumed to apply generally.
 .
 File <lib/ed25519.js> being partially in the Public Domain
 is omitted here:
 Copyright Format 1.0 arguably supports declaring Public Domain
 only when that is the only (non)-license applied.

Files: lib/pkcs1.js
Copyright:
  2013-2014  Digital Bazaar, Inc.
  2003       Ellis Pritchard, Guardian Unlimited <ellis@nukinetics.com)
  2012       Kenji Urushima
License-Grant:
 Distributed under the BSD License.
License: BSD-3-Clause and Expat~Intact

Files: lib/jsbn.js
Copyright:
  2003-2009  Tom Wu
License: Expat~Intact

Files: lib/baseN.js
Copyright:
  2016  base-x contributors
License: Expat

Files: debian/*
Copyright:
  2016-2020, 2022  Jonas Smedegaard <dr@jones.dk>
License-Grant:
 This program is free software;
 you can redistribute it and/or modify it
 under the terms of the GNU General Public License
 as published by the Free Software Foundation;
 either version 3, or (at your option) any later version.
License: GPL-3+
Reference: debian/copyright

License: BSD-3-clause
 Redistribution and use in source and binary forms,
 with or without modification,
 are permitted provided that the following conditions are met:
 .
   * Redistributions of source code must retain
     the above copyright notice, this list of conditions
     and the following disclaimer.
   * Redistributions in binary form must reproduce
     the above copyright notice, this list of conditions
     and the following disclaimer
     in the documentation and/or other materials
     provided with the distribution.
   * Neither the name of highlight.js nor the names of its contributors
     may be used to endorse or promote products
     derived from this software
     without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY
 AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: Expat
 Permission is hereby granted, free of charge,
 to any person obtaining a copy
 of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction,
 including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software,
 and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS",
 WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: Expat~Intact
 Permission is hereby granted, free of charge,
 to any person obtaining a copy
 of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction,
 including without limitation
 the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software,
 and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 .
 The above copyright notice and this permission notice
 shall be included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS-IS"
 AND WITHOUT WARRANTY OF ANY KIND,
 EXPRESS, IMPLIED OR OTHERWISE,
 INCLUDING WITHOUT LIMITATION,
 ANY WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 .
 IN NO EVENT SHALL TOM WU BE LIABLE
 FOR ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL
 DAMAGES OF ANY KIND,
 OR ANY DAMAGES WHATSOEVER
 RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE,
 AND ON ANY THEORY OF LIABILITY,
 ARISING OUT OF OR IN CONNECTION WITH THE USE
 OR PERFORMANCE OF THIS SOFTWARE.
 .
 In addition, the following condition applies:
 .
 All redistributions must retain an intact copy
 of this copyright notice and disclaimer.

License: GPL-2
Reference: /usr/share/common-licenses/GPL-2

License: GPL-3+
Reference: /usr/share/common-licenses/GPL-3
