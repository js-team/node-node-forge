node-node-forge (1.3.1~dfsg-1) UNRELEASED; urgency=medium

  * Team upload
  * Declare compliance with policy 4.6.1
  * New upstream version 1.3.1~dfsg

 -- Yadd <yadd@debian.org>  Mon, 19 Sep 2022 17:00:53 +0200

node-node-forge (1.3.0~dfsg-1) unstable; urgency=medium

  * Team upload

  [ lintian-brush ]
  * Set upstream metadata fields: Security-Contact.

  [ Yadd ]
  * New upstream version 1.3.0~dfsg
    (Closes: CVE-2022-24771, CVE-2022-24772, CVE-2022-24773)

 -- Yadd <yadd@debian.org>  Wed, 23 Mar 2022 10:58:59 +0100

node-node-forge (1.2.1~dfsg-2) unstable; urgency=medium

  * fix have autopkgtest depend on nodejs

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 08 Feb 2022 15:44:30 +0100

node-node-forge (1.2.1~dfsg-1) unstable; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * update patch 2002

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 08 Feb 2022 13:56:53 +0100

node-node-forge (0.10.0~dfsg-4) unstable; urgency=medium

  * update git-buildpackage config:
    + use DEP-14 branch names debian/latest upstream/latest
      (not debian/master upstream)
    + add usage comment
  * declare compliance with Debian Policy 4.6.0
  * use debhelper compatibility level 13 (not 12)
  * support DEB_BUILD_OPTIONS=terse for tests;
    avoid needlessly using prove
  * update generating documentation;
    build-depend on cmark-gfm (not pandoc)
  * update copyright info:
    + use Reference field (not License-Reference);
      update and tighten lintian overrides
    + update coverage
  * fix stop depend on nodejs: interpreter not called from module code

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 08 Feb 2022 13:40:44 +0100

node-node-forge (0.10.0~dfsg-3) unstable; urgency=medium

  [ Debian Janitor ]
  * set Multi-Arch: foreign

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 02 Dec 2020 21:47:37 +0100

node-node-forge (0.10.0~dfsg-2) unstable; urgency=medium

  [ Debian Janitor ]
  * set upstream metadata fields:
    Bug-Database Bug-Submit Repository Repository-Browse
  * fix field name case in debian/copyright (license => License)
  * Apply multi-arch hints.
    + libjs-node-forge, node-node-forge: Add Multi-Arch: foreign.

  [ Jonas Smedegaard ]
  * use brotli compression suffix .brotli
    (not .br used for language breton)
  * declare compliance with Debian Policy 4.5.1

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 02 Dec 2020 21:43:25 +0100

node-node-forge (0.10.0~dfsg-1) unstable; urgency=high

  [ upstream ]
  * new release(s)
    + Remove object path functions.
      closes: bug#969669, thanks to Salvatore Bonaccorso
      CVE-2020-7720

  [ Jonas Smedegaard ]
  * simplify source script copyright-check
  * use debhelper compatibility level 12 (not 10);
    build-depend on debhelper-compat (not debhelper)
  * declare compliance with Debian Policy 4.5.0
  * fix stop needlessly build-depend on perl
  * drop patch 1001, fixed upstream
  * set urgency=high due to security-related fix

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 17 Sep 2020 12:16:19 +0200

node-node-forge (0.9.1~dfsg-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).

  [ Jonas Smedegaard ]
  * Update copyright info: Extend coverage for main copyright holder.
  * Declare compliance with Debian Policy 4.4.1.
  * Use debhelper compatibility level 10.
  * Update watch file: Use dversionmangle=auto.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 22 Oct 2019 12:57:24 +0200

node-node-forge (0.8.5~dfsg-2) unstable; urgency=medium

  * Add patch 1001 to use Webpack 4,
    and tighten to depend on recent webpack.
    Closes: Bug#933592. Thanks to Pirate Praveen.
  * Mark autopkgtest as superficial.
  * Install nodejs code under /usr/share (not /usr/lib).

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 28 Aug 2019 21:46:53 +0200

node-node-forge (0.8.5~dfsg-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).

  [ Jonas Smedegaard ]
  * Declare compliance with Debian Policy 4.4.0.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 14 Jul 2019 22:46:37 -0300

node-node-forge (0.8.1~dfsg-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).

  [ Jonas Smedegaard ]
  * Precompress browser libraries in gzip and brotly formats (not also zlib).
  * Update copyright info: Fix source URLs.
  * Update watch file:
    + Generalize version mangling for repackaging.
    + Simplify regular expressions.
    + Rewrite usage comment.
  * Mark build-dependencies needed only for testsuite as such.
  * Generate (web and now also) plaintext documentation.
    Build-depend on pandoc (not node-marked).

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 26 Feb 2019 14:55:43 +0100

node-node-forge (0.7.6~dfsg-2) unstable; urgency=medium

  * Fix stop bogusly depend on node-source-map node-uglify.
  * Precompress browser libraries in gzip format
    (in addition to zlib and brotli).

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 12 Jan 2019 23:01:31 +0100

node-node-forge (0.7.6~dfsg-1) unstable; urgency=low

  * Initial release.
    Closes: Bug#712219. Thanks to Dominik George.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 12 Jan 2019 21:48:07 +0100
